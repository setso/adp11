package krypto;

public interface IKryptographie {
	
    String decrypt(String krypt);

    String encrypt(String clear);

}
