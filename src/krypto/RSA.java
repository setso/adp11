package krypto;

public class RSA {
	
	int N; //Hauptmodul
	int m; //Nebenmodul
	int e; //Chiffrierexponent
	int d; //Dechiffrierexponent
	
	RSA(){
		
	}
	
	public void init (){
		//create primes
		int prime1 = getRandomThreeDigitPrime();
		System.out.println("prime1 " + prime1);
		int prime2 = getRandomThreeDigitPrime();
		System.out.println("prime2 " + prime2);
				
		//Hauptmodul berechnen
		N = calculateHauptmodul(prime1, prime2);
		System.out.println("Hauptmodul " + N);
				
		//Nebenmodul berechnen
		m = calculateNebenmodul(prime1, prime2);
		System.out.println("Nebenmodul " + m);
			
		//Teilerfremde Zahl finden --> Chiffrierexponent 
		e = calculateChiffrierexponent(m);
			
		//Dechiffrierexponent bestimmen
		d= calculateDehiffrierexponent(e, m);
	}
	
	public int calculateHauptmodul(int prime1, int prime2){
		return prime1*prime2;
	}
	
	public int calculateNebenmodul(int prime1, int prime2){
		return (prime1-1)*(prime2-1);
	}
	
	public int calculateChiffrierexponent(int nebenmodul){
		int _e = 3;
		while (euklid(nebenmodul, _e)!=1){
		_e++;
		}
		System.out.println("Chiffrierexponent " + _e);
		return _e;
	}
	
	public int calculateDehiffrierexponent(int chiffrierexponent, int nebenmodul){
		int _e=chiffrierexponent;
		int _m=nebenmodul;
		int _d=1;
		while((_e*_d)%_m!=1){
		_d++;
		}
		System.out.println("Dechiffrierexponent " + _d);
		return _d;
	}

	/**
	 * 
	 * @param message
	 * @param hauptmodul
	 * @param chipherexponent
	 * @return
	 */
	public long chipher(int message, int hauptmodul ,int chipherexponent){
		long _message=message;
		
		for (int i =1; i< chipherexponent; i++){
			_message=((message*_message)%hauptmodul);
		}
		return _message;
		
	}
	
	/**
	 * 
	 * @param message
	 * @param nebenmodul
	 * @param dechipherexponent
	 * @return
	 */
	public long dechipher(long message, int hauptmodul ,int dechipherexponent){
		long _message=message;
		
		for (int i =1; i< dechipherexponent; i++){
			_message=((message*_message)%hauptmodul);
		}
		return _message;
	}
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public int euklid (int a, int b){
		if (b == 0)
		{
			return a;
		}
		else return euklid (b, a%b);
	}

	
	public int getHauptmodul(){
		return N;
	}
	
	public void setHauptmodul(int _N){
		N=_N;
	}
	
	public int getNebenmodul(){
		return m;
	}
	
	public void setNebenmodul(int _m){
		m=_m;
	}
	
	public int getChiffrierexponent(){
		return e;
	}
	
	public void setChiffrierexponent(int _e){
		e=_e;
	}
	
	public int getDechiffrierexponent(){
		return d;
	}
	
	public void setDechiffrierexponent(int _d){
		d=_d;
	}
	
	
	/**
	 * 
	 * @return A random three digit prime number
	 */
	public int getRandomThreeDigitPrime() {
		int [] primes = {	101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 
							179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257,   
							263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349,
							353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439,
							443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541,
							547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631,
							641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733,
							739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 
							839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 
							947, 953, 967, 971, 977, 983, 991, 997};
		return primes[(int)(Math.random()*primes.length)];
	}
	

}
