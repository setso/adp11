package krypto;

public class SymmetricBlockCipher implements IKryptographie{

	/**
	 * 
	 */
//	@Override
//	public String decrypt(String _krypt) {
//		int intKryptArray[]= new int[_krypt.length()/3];
//		
//		//Read and convert String
//		for (int i=0; i<_krypt.length(); i=i+3){
//			String _seq = _krypt.substring(i, i+3);
//			int _seqInt = Integer.parseInt(_seq);
//			intKryptArray[i/3]=_seqInt;
//		}
//		
//		//Get session keys
//		int s0=intKryptArray[0];
//		int s1=intKryptArray[1];
//		
//		int [] intClearArray = new int[intKryptArray.length-2];
//		
//		//Decrypt array
//		for (int i=0;i <intKryptArray.length-2; i=i+2){
//			intClearArray[i]=(intKryptArray[i+2]-s0)%95;
//			
//			//Special clause for strings with odd length
//			if (i+1<intClearArray.length){
//				intClearArray[i+1]=(intKryptArray[i+3]-s1)%95;
//			}
//			
//		}
//		
//		
//		char [] charClearArray = new char[intClearArray.length];
//		for (int i=0; i < intClearArray.length; i++) {
//			if (intClearArray[i]<=0){
//				charClearArray[i]=(char) (intClearArray[i]+32);
//			}
//			else {
//				charClearArray[i]=(char) (intClearArray[i]+32);
//			}
//		}
//		
//		String decryptedString = "";
//		
//		for (int i=0; i <intClearArray.length; i++){
//			decryptedString = decryptedString + charClearArray[i];
//		}
//		System.out.println("Finished decryption");
//			
//		return decryptedString;
//		
//	}
//
//	/**
//	 * 
//	 */
//	@Override
//	public String encrypt(String _clear) {
//		int intClearArray[];
//		int intKryptArray[];
//		char charClearArray[];
//		
//		charClearArray = _clear.toCharArray();
//		intClearArray = new int[charClearArray.length];
//		
//		//Convert to int
//		for (int i=0; i < intClearArray.length; i++){
//			intClearArray[i]=(int)charClearArray[i]-32;
//		}
//		
//		//Create Session keys
//		int s0 = (int) (Math.random()*95);
//		int s1 = (int) (Math.random()*95);
//		//intKryptArray=new int[intClearArray.length+2];
//		intKryptArray=new int[intClearArray.length+8];
//		intKryptArray[0]=s0;
//		intKryptArray[1]=s1;
//		
//		//Encrypt array
//		for (int i=2;i <intClearArray.length+8; i=i+2){
//			intKryptArray[i+8]=(intClearArray[i])+(s0%95);
//			intKryptArray[i+8+1]=(intClearArray[i+1])+(s1%95);
//			
//			//Special clause for strings with odd length
////			if (i+1<intKryptArray.length) {
////				intKryptArray[i+1]=(intClearArray[i-1])+(s1%95);
////			}
//		}
//		
//
//	
//		//Generate output string
//		String intKryptString="";
//		for (int x=0; x < intKryptArray.length; x++){
//			if (intKryptArray[x]<10){
//				intKryptString=intKryptString + '0'+ '0' + intKryptArray[x];
//			}
//			else if (intKryptArray[x]>=10&&intKryptArray[x]<100){
//				intKryptString=intKryptString + '0' + intKryptArray[x];
//			}
//			else {
//				intKryptString=intKryptString + intKryptArray[x];
//			}
//		}
//		
//		System.out.println("KryptString length " + intKryptString.length());
//		return intKryptString;
//	}

	@Override
    public String encrypt(String toDecrypt) {
        char charClearArray[] = toDecrypt.toCharArray();
        int intClearArray[] = new int[toDecrypt.length()];

        for (int i = 0; i < charClearArray.length; i++) {
            intClearArray[i] = charClearArray[i] - 32;
        }
        int s0 = (int) (Math.random() * 94) + 1;
        int s1 = (int) (Math.random() * 94) + 1;

        int intKryptArray[] = new int[charClearArray.length + 8];

        intKryptArray[0] = s0;
        intKryptArray[1] = s1;

        for (int i = 0; i < charClearArray.length - 1; i += 2) {
            intKryptArray[i + 8] = (intClearArray[i] + s0) % 95;
            intKryptArray[i + 1 + 8] = (intClearArray[i + 1] + s1) % 95;
        }

        if (toDecrypt.length() % 2 != 0) {
            intKryptArray[charClearArray.length - 1 + 8] = (intClearArray[charClearArray.length - 1] + s0) % 95;
        }

        char charKryptArray[] = new char[intKryptArray.length];

        for (int i = 0; i < intKryptArray.length; i++) {
            charKryptArray[i] = (char) (intKryptArray[i] + 32);
        }

        return String.valueOf(charKryptArray);
    }

    @Override
    public String decrypt(String toEncrypt) {
        char charKryptArray[] = toEncrypt.toCharArray();
        int intKryptArray[] = new int[toEncrypt.length()];

        for (int i = 0; i < charKryptArray.length; i++) {
            intKryptArray[i] = charKryptArray[i] - 32;
        }

        int s0 = intKryptArray[0];
        int s1 = intKryptArray[1];
        char charClearArray[] = new char[intKryptArray.length - 8];

        for (int i = 0; i < charClearArray.length - 1; i += 2) {
            if (s0 > intKryptArray[i + 8])
                charClearArray[i] = (char) (intKryptArray[i + 8] + 95 - s0 + 32);
            else
                charClearArray[i] = (char) (intKryptArray[i + 8] - s0 + 32);

            if (s1 > intKryptArray[i + 1 + 8])
                charClearArray[i + 1] = (char) (intKryptArray[i + 1 + 8] + 95 - s1 + 32);
            else
                charClearArray[i + 1] = (char) (intKryptArray[i + 1 + 8] - s1 + 32);
        }

        if (intKryptArray.length % 2 != 0) {
            if (s0 > intKryptArray[charClearArray.length - 1 + 8])
                charClearArray[charClearArray.length - 1] = (char) (intKryptArray[charClearArray.length - 1 + 8] + 95 - s0 + 32);
            else
                charClearArray[charClearArray.length - 1] = (char) (intKryptArray[charClearArray.length - 1 + 8] - s0 + 32);
        }
        return String.valueOf(charClearArray);
    }

}
