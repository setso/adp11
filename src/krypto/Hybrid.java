package krypto;

public class Hybrid{
    private SymmetricBlockCipher symmetricalCrypted;
    private RSA rsa;

    public Hybrid() {
        symmetricalCrypted = new SymmetricBlockCipher();
        rsa = new RSA();
    }
    
    
    public String encrypt(String clear, int publicexponent, int hauptmodul) {
        String cryptedText = symmetricalCrypted.encrypt(clear);
        char charKryptArray[] = cryptedText.toCharArray();
        int intKryptArray[] = new int[cryptedText.length()];

        for (int i = 0; i < charKryptArray.length; i++) {
            intKryptArray[i] = charKryptArray[i] - 32;
        }
        
        int sessionKey_0 = (int) rsa.dechipher(intKryptArray[0], hauptmodul, publicexponent);
        int sessionKey_1 = (int) rsa.dechipher(intKryptArray[1], hauptmodul, publicexponent);
        intKryptArray[0] = sessionKey_0 / (95 * 95 * 95);
        intKryptArray[1] = (sessionKey_0 % (95 * 95 * 95)) / (95 * 95);
        intKryptArray[2] = (sessionKey_0 % (95 * 95)) / 95;
        intKryptArray[3] = sessionKey_0 % 95;

        intKryptArray[4] = sessionKey_1 / (95 * 95 * 95);
        intKryptArray[5] = (sessionKey_1 % (95 * 95 * 95)) / (95 * 95);
        intKryptArray[6] = (sessionKey_1 % (95 * 95)) / 95;
        intKryptArray[7] = sessionKey_1 % 95;

        char decryptedCharArray[] = new char[intKryptArray.length];

        for (int i = 0; i < intKryptArray.length; i++) {
            decryptedCharArray[i] = (char) (intKryptArray[i] + 32);
        }

        return String.valueOf(decryptedCharArray);
    }
    

    public String decrypt(String crypt, int privateexponent, int hauptmodul) {
        char charKryptArray[] = crypt.toCharArray();

        int[] intKryptArray = new int[charKryptArray.length];
        for (int i = 0; i < charKryptArray.length; i++) {
            intKryptArray[i] = charKryptArray[i] - 32;
        }

        int sessionKey_0 = intKryptArray[3];
        sessionKey_0 += (intKryptArray[2] * 95) % (95 * 95);
        sessionKey_0 += (intKryptArray[1] * 95 * 95) % (95 * 95 * 95);
        sessionKey_0 += (intKryptArray[0] * 95 * 95 * 95);

        int sessionKey_1 = intKryptArray[7];
        sessionKey_1 += (intKryptArray[6] * 95) % (95 * 95);
        sessionKey_1 += (intKryptArray[5] * 95 * 95) % (95 * 95 * 95);
        sessionKey_1 += (intKryptArray[4] * 95 * 95 * 95);

        int s0 = (int) rsa.chipher(sessionKey_0, hauptmodul, privateexponent);
        int s1 = (int) rsa.chipher(sessionKey_1, hauptmodul, privateexponent);

        intKryptArray[0] = s0;
        intKryptArray[1] = s1;

        char charKryptArrayReturn[] = new char[intKryptArray.length];

        for (int i = 0; i < intKryptArray.length; i++) {
            charKryptArrayReturn[i] = (char) (intKryptArray[i] + 32);
        }
        String clear = symmetricalCrypted.decrypt(String.valueOf(charKryptArrayReturn));
        return clear;
    }

}

