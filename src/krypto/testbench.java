package krypto;

public class testbench {

	public static void main(String [] args){
		
//		SymmetricBlockCipher symBlockCyph = new SymmetricBlockCipher();
		String message = ("ADP Praktikum Aufgabenblatt 11");
		System.out.println("Message: "+ message);
//		String encryptedString = symBlockCyph.encrypt(message);
//		System.out.println("Encrypted String " + encryptedString);
//		String decryptedString = symBlockCyph.decrypt(encryptedString);
//		System.out.println("Decrypted String " + decryptedString);
//		
//		int hauptmodul = rsa.getHauptmodul();
//		int chiffrierexponent = rsa.getChiffrierexponent();
//		int nebenmodul = rsa.getNebenmodul();
//		int dechiffrierexponent = rsa.getDechiffrierexponent();
		
//		int encrypted = rsa.chipher(42, hauptmodul, chiffrierexponent);
//		System.out.println("Encrypted " + encrypted);		
//		int decrypted = rsa.dechipher(encrypted, nebenmodul , dechiffrierexponent);
//		System.out.println("Decrypted " + decrypted);
		
//		int encrypted = rsa.chipher(42, 55, 3);
//		System.out.println("Encrypted " + encrypted);
//		
//		int decrypted = rsa.dechipher(encrypted, 55, 27);
//		System.out.println("Decrypted " + decrypted);
		
		System.out.println("Asymmetric");
		System.out.println("___________________________________");
		RSA rsa = new RSA();
		int mes = 42;
		System.out.println("Encrypting mes: " + mes);
		long encrypted = rsa.chipher(mes, 86609, 17);
		System.out.println("Encrypted " + encrypted);	
		
		long decrypted = rsa.dechipher(encrypted, 86609, 65777);
		System.out.println("Decrypted " + decrypted);
		
		System.out.println();System.out.println();
		System.out.println("Symmetric");
		System.out.println("___________________________________");
		SymmetricBlockCipher sbc = new SymmetricBlockCipher();
		
		String sclear = "blub";
		System.out.println("Encrypting mes: " + sclear);
		String sencrypt= sbc.encrypt("Blub");
		System.out.println("Encrypted " + sencrypt);	
		System.out.println("Decrypted " + sbc.decrypt(sencrypt));
		
		
		System.out.println();System.out.println();
		System.out.println("Hybrid");
		System.out.println("___________________________________");
		Hybrid hybrid = new Hybrid();
		String hclear = "Algorithmen und Datenstrukturen.";
		
		System.out.println("Encrypting mes: " + hclear);
		String hencrypt= hybrid.encrypt(hclear, 143021, 358871);
		System.out.println("Encrypted " + hencrypt);	
		
		System.out.println("Decrypted " + hybrid.decrypt(hencrypt, 5,358871));
		

		
	}

}
