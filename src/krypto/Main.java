package krypto;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
 
public class Main extends Application {
	
	static RSA rsa;
 	static SymmetricBlockCipher symBlockCyph;
 	static Hybrid hybrid;
 	

	Text tprime1;
	Text tprime2;
	Text tmainmodulus;
	Text tsidemodulus;
	Text tprivateexp;
	Text tpublicexp;
	Text keypriv;
	Text keypub;
	
	TextArea tfEncString1;
	TextArea tfDecString1;
	TextArea tfEncString2;
	TextArea tfDecString2;
	TextArea tfEncString3;
	TextArea tfDecString3;
	TextArea tfEncString4;
	TextArea tfDecString4;
	int prime1;
	int prime2;
	int mainmodulus;
	int sidemodulus;
	int privateexp;
	int publicexp;
	
	TextArea tfclearMessage1;
	TextArea tfcryptMessage1;
	TextArea tfclearMessage2;
	TextArea tfcryptMessage2;
	
	TextField tHybrEncExp;
	TextField tHybrEncModul;
	
	TextField tHybrDecExp;
	TextField tHybrDecModul;
	
	public static void main(String[] args) {
		rsa = new RSA();
	 	symBlockCyph = new SymmetricBlockCipher();
	 	hybrid = new Hybrid();
        launch(args);
    }
	
    @Override
    public void start(Stage primaryStage) {
    	   	   	
    	GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));
        
        Font simpletext = Font.font("Arial", FontWeight.NORMAL, 16);
              		
        
        // ASYMMETRIC IMPLEMENTATION
        Text title1 = new Text("Asymmetric");
        title1.setFont(Font.font("Arial", FontWeight.BOLD, 22));
        title1.setFill(Color.WHITE);
        grid.add(title1, 1, 0); 
       
        //Generate primes
        Button btnGenPrimes = new Button();
        btnGenPrimes.setText("Generate primes");
        btnGenPrimes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                prime1 = rsa.getRandomThreeDigitPrime();
                tprime1.setText(String.valueOf(prime1));
                
                prime2 = rsa.getRandomThreeDigitPrime();
                tprime2.setText(String.valueOf(prime2));
            }
        });
        btnGenPrimes.setPrefWidth(160);
        grid.add(btnGenPrimes, 0, 1);
        
        tprime1 = new Text("0000");
        tprime1.setFont(simpletext);
        tprime1.setFill(Color.WHITE);
        tprime1.setTextAlignment(TextAlignment.RIGHT);
        grid.add(tprime1, 1, 1); 
        
        
        Text tp1= new Text("<- prime1");
        tp1.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tp1.setFill(Color.WHITE);
        
        Text tp2= new Text("prime2 ->");
        tp2.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tp2.setFill(Color.WHITE);
                
        HBox hbPrimes = new HBox();
        hbPrimes.getChildren().addAll(tp1, tp2);
        hbPrimes.setSpacing(20);
        grid.add(hbPrimes, 2, 1); 
        
        tprime2 = new Text("0000");
        tprime2.setFont(simpletext);
        tprime2.setFill(Color.WHITE);
        grid.add(tprime2, 3, 1); 
        
        //Calculate modulus
        Button btnCalcMainModul = new Button();
        btnCalcMainModul.setText("Calcluate modulus");
        btnCalcMainModul.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainmodulus = rsa.calculateHauptmodul(prime1, prime2);
                tmainmodulus.setText(String.valueOf(mainmodulus));
                
                sidemodulus = rsa.calculateNebenmodul(prime1, prime2);
                tsidemodulus.setText(String.valueOf(sidemodulus));
            }
        });
        btnCalcMainModul.setPrefWidth(160);
        grid.add(btnCalcMainModul, 0, 2);
        
        Text tm= new Text("<- main");
        tm.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tm.setFill(Color.WHITE);
        
        Text ts= new Text("side ->");
        ts.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        ts.setFill(Color.WHITE);
        
        HBox hbModulus = new HBox();
        hbModulus.getChildren().addAll(tm, ts);
        hbModulus.setSpacing(20);
        grid.add(hbModulus, 2, 2); 
        
        tmainmodulus = new Text("0000");
        tmainmodulus.setFont(simpletext);
        tmainmodulus.setFill(Color.WHITE);
        grid.add(tmainmodulus, 1, 2); 
        
        tsidemodulus = new Text("0000");
        tsidemodulus.setFont(simpletext);
        tsidemodulus.setFill(Color.WHITE);
        grid.add(tsidemodulus, 3, 2);
        
        //Generate keys
        Button btnGenKeys = new Button();
        btnGenKeys.setText("Generate Keys");
        btnGenKeys.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                privateexp = rsa.calculateChiffrierexponent(sidemodulus);
                tprivateexp.setText(String.valueOf(privateexp));
                
                publicexp = rsa.calculateDehiffrierexponent(privateexp, sidemodulus);
                tpublicexp.setText(String.valueOf(publicexp));
                
                keypub.setText("("+String.valueOf(publicexp)+", "+String.valueOf(mainmodulus)+")");
                keypriv.setText("("+ String.valueOf(privateexp)+", "+String.valueOf(mainmodulus)+")");
                
            }
        });
        btnGenKeys.setPrefWidth(160);
        grid.add(btnGenKeys, 0, 3);
        
        Text tprivk= new Text("<- private");
        tprivk.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tprivk.setFill(Color.WHITE);
        
        Text tpubk= new Text("public ->");
        tpubk.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tpubk.setFill(Color.WHITE);
        
        HBox hbKeys = new HBox();
        hbKeys.getChildren().addAll(tprivk, tpubk);
        hbKeys.setSpacing(20);
        grid.add(hbKeys, 2, 3);
        
        tprivateexp= new Text("0000");
        tprivateexp.setFont(simpletext);
        tprivateexp.setFill(Color.WHITE);
        grid.add(tprivateexp, 1, 3); 
        
        tpublicexp= new Text("0000");
        tpublicexp.setFont(simpletext);
        tpublicexp.setFill(Color.WHITE);
        grid.add(tpublicexp, 3, 3); 
        
        //Encrypt with public key
        Button btnEncr1= new Button();
        btnEncr1.setText("Encrypt String");
        btnEncr1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	int clearmessage = Integer.parseInt(tfDecString3.getText());
            	if (clearmessage>=mainmodulus) {
            		Alert alert = new Alert(AlertType.ERROR);
                	alert.setTitle("Error");
                	alert.setHeaderText("Input Error");
                	alert.setContentText("Input too long");
                	alert.showAndWait();
            	}
            	long cryptmessage = rsa.chipher(clearmessage, mainmodulus, publicexp);
                tfEncString3.setText(String.valueOf(cryptmessage));  
            }
        });
        btnEncr1.setPrefWidth(160);
        grid.add(btnEncr1, 0, 4);
        
        Text tenc3= new Text("<- Input");
        tenc3.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tenc3.setFill(Color.WHITE);
        
        Text tdec3= new Text("Output ->");
        tdec3.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tdec3.setFill(Color.WHITE);
        
        HBox hb1 = new HBox();
        hb1.getChildren().addAll(tenc3, tdec3);
        hb1.setSpacing(40);
        grid.add(hb1, 2, 4);
        
        tfDecString3 = new TextArea ();
        tfDecString3.setPromptText("Number to encrypt with public key");
        tfDecString3.setWrapText(true);
        tfDecString3.setPrefSize(180, 40);
        grid.add(tfDecString3, 1, 4);
                
        tfEncString3 = new TextArea ();
        tfEncString3.setPromptText("Encrypted String");
        tfEncString3.setWrapText(true);
        tfEncString3.setPrefSize(180, 40);
        grid.add(tfEncString3, 3, 4);
        
        //Decrypt with public key
        Button btnDecr2= new Button();
        btnDecr2.setText("Decrypt String");
        btnDecr2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	int cryptmessage = Integer.parseInt(tfEncString4.getText());
            	long clearmessage = rsa.dechipher(cryptmessage, mainmodulus, privateexp);
                tfDecString4.setText(String.valueOf(clearmessage));  
            }
        });
        btnDecr2.setPrefWidth(160);
        grid.add(btnDecr2, 0, 5);
        
        Text tenc4= new Text("<- Input");
        tenc4.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tenc4.setFill(Color.WHITE);
        
        Text tdec4= new Text("Output ->");
        tdec4.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tdec4.setFill(Color.WHITE);
        
        HBox hb2 = new HBox();
        hb2.getChildren().addAll(tenc4, tdec4);
        hb2.setSpacing(40);
        grid.add(hb2, 2, 5);
        
        tfEncString4 = new TextArea ();
        tfEncString4.setPromptText("String to decrypt with private key");
        tfEncString4.setWrapText(true);
        tfEncString4.setPrefSize(180, 40);
        grid.add(tfEncString4, 1, 5);
                
        tfDecString4 = new TextArea ();
        tfDecString4.setPromptText("Encrypted String");
        tfDecString4.setWrapText(true);
        tfDecString4.setPrefSize(180, 40);
        grid.add(tfDecString4, 3, 5);

        keypriv= new Text("Private Key");
        keypriv.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        keypriv.setFill(Color.WHITE);
        grid.add(keypriv, 1, 6);
        
        keypub= new Text("Public Key");
        keypub.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        keypub.setFill(Color.WHITE);
        grid.add(keypub, 3, 6);
        
        Text tkeypriv= new Text("<- Private Key");
        tkeypriv.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tkeypriv.setFill(Color.WHITE);
        
        Text tkeypub= new Text("Public Key ->");
        tkeypub.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tkeypub.setFill(Color.WHITE);
        
        HBox hbkey = new HBox();
        hbkey.getChildren().addAll(tkeypriv, tkeypub);
        hbkey.setSpacing(12);
        grid.add(hbkey, 2, 6);
        
        /*
         * 
         * SYMMETRIC IMPLEMENTATION
         * 
         */
        Text title2 = new Text("Symmetric");
        title2.setFont(Font.font("Arial", FontWeight.BOLD, 22));
        title2.setFill(Color.WHITE);
        grid.add(title2, 1, 7); 
       
        //Encrypt
        Button btnEncr= new Button();
        btnEncr.setText("Encrypt String");
        btnEncr.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String senc = symBlockCyph.encrypt(tfEncString1.getText());
                tfDecString1.setText(senc);  
            }
        });
        btnEncr.setPrefWidth(160);
        grid.add(btnEncr, 0, 8);
        
        Text tenc1= new Text("<- Input");
        tenc1.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tenc1.setFill(Color.WHITE);
        
        Text tdec1= new Text("Output ->");
        tdec1.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tdec1.setFill(Color.WHITE);
        
        HBox hb3 = new HBox();
        hb3.getChildren().addAll(tenc1, tdec1);
        hb3.setSpacing(40);
        grid.add(hb3, 2, 8);
        
        tfEncString1 = new TextArea ();
        tfEncString1.setPromptText("String to encrypt");
        tfEncString1.setWrapText(true);
        tfEncString1.setPrefSize(180, 40);
        grid.add(tfEncString1, 1, 8);
               
        tfDecString1 = new TextArea ();
        tfDecString1.setPromptText("Encrypted String");
        tfDecString1.setWrapText(true);
        tfDecString1.setPrefSize(180, 40);
        grid.add(tfDecString1, 3, 8);
        
        //Decrypt
        Button btnDecr= new Button();
        btnDecr.setText("Decrypt String");
        btnDecr.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String senc = symBlockCyph.decrypt(tfEncString2.getText());
                tfDecString2.setText(senc);  
            }
        });
        btnDecr.setPrefWidth(160);
        grid.add(btnDecr, 0, 9);
        
        Text tenc2= new Text("<- Input");
        tenc2.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tenc2.setFill(Color.WHITE);
        
        Text tdec2= new Text("Output ->");
        tdec2.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tdec2.setFill(Color.WHITE);
        
        HBox hb4 = new HBox();
        hb4.getChildren().addAll(tenc2, tdec2);
        hb4.setSpacing(40);
        grid.add(hb4, 2, 9);
        
        tfEncString2 = new TextArea ();
        tfEncString2.setPromptText("String to decrypt");
        tfEncString2.setWrapText(true);
        tfEncString2.setPrefSize(180, 40);
        grid.add(tfEncString2, 1, 9);
        
        
        
        tfDecString2 = new TextArea ();
        tfDecString2.setPromptText("Decrypted String");
        tfDecString2.setWrapText(true);
        tfDecString2.setPrefSize(180, 40);
        grid.add(tfDecString2, 3, 9);

        /*
         * 
         * HYBRID IMPLEMENTATION
         * 
         */
        Text title3 = new Text("Hybrid");
        title3.setFont(Font.font("Arial", FontWeight.BOLD, 22));
        title3.setFill(Color.WHITE);
        grid.add(title3, 1, 10); 
        
        //Encrypt
        Button btnHybrEncr= new Button();
        btnHybrEncr.setText("Encrypt");
        btnHybrEncr.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	String clearMessage = tfclearMessage1.getText();
            	String hybrCryptMessage = hybrid.encrypt(clearMessage, Integer.parseInt(tHybrEncExp.getText()), Integer.parseInt(tHybrEncModul.getText()));
            	tfcryptMessage1.setText(hybrCryptMessage);
            }
        });
        btnHybrEncr.setPrefWidth(160);
        grid.add(btnHybrEncr, 0, 11);
        
        //Key
        Text tHybrEncKey= new Text("Key");
        tHybrEncKey.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tHybrEncKey.setFill(Color.WHITE);
        grid.add(tHybrEncKey, 1, 11); 

        tHybrEncExp= new TextField();
        tHybrEncExp.setPromptText("Public Key");
        tHybrEncExp.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        
        tHybrEncModul= new TextField();
        tHybrEncModul.setPromptText("Module");
        tHybrEncModul.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        
        VBox vbEnc = new VBox();
        vbEnc.getChildren().addAll(tHybrEncExp, tHybrEncModul);
        vbEnc.setSpacing(16);
        grid.add(vbEnc, 1, 12); 
        
        //Clear Message
        Text tClearMessage1= new Text("Clear Message");
        tClearMessage1.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tClearMessage1.setFill(Color.WHITE);
        grid.add(tClearMessage1, 2, 11); 
        
        tfclearMessage1 = new TextArea ();
        tfclearMessage1.setWrapText(true);
        tfclearMessage1.setPrefSize(180, 80);
        grid.add(tfclearMessage1, 2, 12);
        
        Text tCryptMessage1= new Text("Crypt Message");
        tCryptMessage1.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tCryptMessage1.setFill(Color.WHITE);
        grid.add(tCryptMessage1, 3, 11); 
        
        tfcryptMessage1 = new TextArea ();
        tfcryptMessage1.setWrapText(true);
        tfcryptMessage1.setPrefSize(180, 80);
        grid.add(tfcryptMessage1, 3, 12);
        
        //DECRYPT
        Button btnHybrDecr= new Button();
        btnHybrDecr.setText("Decrypt");
        btnHybrDecr.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {          	
                String cryptMessage = tfcryptMessage2.getText();
            	String hybrclearMessage = hybrid.decrypt(cryptMessage, Integer.parseInt(tHybrDecExp.getText()), Integer.parseInt(tHybrDecModul.getText()));
            	tfclearMessage2.setText(hybrclearMessage);
            	
            }
        });
        btnHybrDecr.setPrefWidth(160);
        grid.add(btnHybrDecr, 0, 14);
        
        //Key
        Text tHybrDecrKey= new Text("Key");
        tHybrDecrKey.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tHybrDecrKey.setFill(Color.WHITE);
        grid.add(tHybrDecrKey, 1, 14); 
        
        tHybrDecExp= new TextField();
        tHybrDecExp.setPromptText("Private Key");
        tHybrDecExp.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        
        tHybrDecModul= new TextField();
        tHybrDecModul.setPromptText("Module");
        tHybrDecModul.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
     
        
        VBox vbDec = new VBox();
        vbDec.getChildren().addAll(tHybrDecExp, tHybrDecModul);
        vbDec.setSpacing(16);
        grid.add(vbDec, 1, 15); 
        
        
        //Crypt Message
        Text tCryptMessage2= new Text("Crypt Message");
        tCryptMessage2.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tCryptMessage2.setFill(Color.WHITE);
        grid.add(tCryptMessage2, 2, 14); 
        
        tfcryptMessage2 = new TextArea ();
        tfcryptMessage2.setWrapText(true);
        tfcryptMessage2.setPrefSize(180, 80);
        grid.add(tfcryptMessage2, 2, 15);
        
        //Clear Message
        Text tClearMessage2= new Text("Clear Message");
        tClearMessage2.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        tClearMessage2.setFill(Color.WHITE);
        grid.add(tClearMessage2, 3, 14); 
        
        tfclearMessage2 = new TextArea ();
        tfclearMessage2.setWrapText(true);
        tfclearMessage2.setPrefSize(180, 80);
        grid.add(tfclearMessage2, 3, 15);
        
        
        //Background
        BackgroundImage bg = new BackgroundImage(new Image("/krypto/bg.jpg"), BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, 
        		BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        grid.setBackground(new Background(bg));
        
        
        Scene scene = new Scene(grid, 820, 780);
        scene.getStylesheets().add("/krypto/style.css");
        
        primaryStage.setTitle("Algorithmen und Datenstrukturen - Kryptographie");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    

 	
}